Spring Microservices:
	CQRS design, Event Sourcing design, Axion server/framwworfk
	Event driven microservices?
	sl4j vs log4j2
	Patch vs PUT action method
	.props vs yml fie
	Do we need to have all apps having same microservice version, also service regstry, api gateway
	spring.start io not shwoing hystrix
	compatiblility fro spring boot spring clod versions, Learn names of verisons
	check: Spring Cloud Config Client has changed and technically bootstrap.properties and bootstrap.yml files are deprecated.??
	Study deprecation props yml files
	Do we need to re-start the cloud-config-server or microservices after updating application.yml file in git?
	Cloud-config-server is not fetching from own use props from GIT. OR MAy be we need to add in bootstrapp.yml
	
	How common properties we used as config-server, need restart or not, scenarios with multiple services with replica of each microservice. HOW?
	CENTRALIZED CONFIGURATION?
	
Daily code buffer:
	Department Service,User Service, API gateway, Service registry, Config server[Git Repo], Hystrix, Zipkin server, Sleuth
	Read spring-cloud dependencies
	
	Steps:
		1. Department-Service
			1. Create spring boot app department-service with spring-cloud dependency to be as a eureka client. @EnableEurekaClient
			2. Create Controller/Repo/Service/Entity with oeprations
			3. For registered to eureka server update application.yml
					spring:
					  application:
						name: DEPARTMENT-SERVICE
					eureka:
					  client:
						register-with-eureka: true
						fetch-registry: true
						service-url:
						  defaultZone: http://localhost:8761/eureka/
					  instance:
						hostname: localhost
		2. User-Service
			1. Create spring boot app user-service with spring-cloud dependency to be as a eureka client. @EnableEurekaClient
			2. Create Controller/Repo/Service/Entity with oeprations. Here we will also call department-service using http://DEPARTMENT-SERVICE/departments/
				This will only works if service-registry application is running and all department/users service isntances are associated
			3. Create Bean RestTemplate with @LoadBalance else it would throw UnknownHost exception if you calls DEPARTMENT-API url which is registered in User-service
			4. Update application.yml similar to user-service
		3. Service-Registry:
			1. Create spring boot app service-registry with spring-cloud dependency to be as a eureka server.
			2. @EnableEurekaServer in main application classs.
			3. pom dependency of netflix-eureka-server instead of eureka-client
			4. application.yml
				eureka:
				  client:
					register-with-eureka: false
					fetch-registry: false
		After 3 steps done we can call department service from user-service without hardcoded the urls and using DEPARTMENT-SERVICE as registered in service regstry
		4. API Cloudway: All apis can work on 9191, We don't need to call the mincroservice. Only calls start from API GAteway
			1. As eureka client and registedred to eureka server so dependency eureka client + starter-gateway of cloud.  @EnableEurekaClient
			2. application.yml with same above plus with cloudgateway settings
				spring:
				  application:
					name: API-GATEWAY
				  cloud:
					gateway:
					  routes:
						- id: USERS-SERVICE
						  uri: lb://USERS-SERVICE
						  predicates:
							- Path=/users/**
						- id: DEPARTMENT-SERVICE
						  uri: lb://DEPARTMENT-SERVICE
						  predicates:
							- Path=/departments/**
		Here after these steps we can directly calls the API gateway.
		5. Hystrix library & dashobard: Circuit breaker + fallback methods (fault tolerance library)
			Hystrix is not showing in spring boot version <version>2.5.9</version>
			Seems Like hystrix deprecated or not available for latest spring boot versions or spring cloud versions
			-> Api Gatway : 
				-> Add dependency of hystrix, @EnableHystrix, EnableEurekaCLient
				-> Create rest controller FallBackMethodController, with fall back methods
				-> application.yml:
					1. Add filters.circuitbreaker for user-department service so that when fails happen it sends to fallback methods
					#It will call fallback methods after timeout of 4 seconds
						hystrix:
						  command:
							fallbackcmd:
							  execution:
								isolation:
								  thread:
									timeoutInMilliseconds: 4000


						#So that we can send these informations to Hystrix dashobard
						management:
						  endpoints:
							web:
							  exposure:
								include: hystrix.stream
		6. Hystrix-Dashboard:
			Create new spring module as eureka client + dependency of hystrix-dashboard
			@EnableHystrixDashboard @EnableEurekaClient
			application.yml: SO that from cloudway we can get the monitor the hystrix.stream stream
				hystrix:
				  dashboard:
					proxy-stream-allow-list: "*"
		7. Now how to run hystrix stream + hystrix dashboard
			1. localhost:9191/actuator/hystrix.stream
			2. localhost:9297/hystrix add stream path. It will show the hystrix dashboard
			3. Here if we stop the department service and hit user or department service it will show us the fallback methods response
		8. Config-server: Git : 
			As we can see we have hardcoded eureka server name in the application.yml in all the microservices including service-registry + api gateway.
			So what happends if this changes.
			1. Create new spring boot application with eureak client + Config server dependency
			2. Add dependency spring-cloud-starter-config in all microservices.
			3. department-service and all : application.yml add cloud:config:enabled:true:uri:http://localhost:9296 which is cloud-config server port
			4. Now application.yml is for application-context, but for getting all the cloud config we need bootstrap.yml file to bootstrap app and the context.
			5. Add bootstrap.yml file add only spring-cloud-config. So remove this in application.yml
			6. Also remove the eureka:client details from application.yml because it will picked up from git now from bootstrap
			7. No need to add bootstrap for service-registry + cloud-server as these are not registring to eureka server.
			8. Start service-registry -> cloud-config-server -> api-gateway -> 
			9. Issue facing as we are using latest spring-boot/cloud so need to add below step
				<!-- Facing issues while configuring cloud-server
					https://stackoverflow.com/questions/67507452/no-spring-config-import-property-has-been-defined -->
				<dependency>
					<groupId>org.springframework.cloud</groupId>
					<artifactId>spring-cloud-starter-bootstrap</artifactId>
				</dependency>
			10. Now all apps works fine and registered with eureka server, and eureka server port coming from bitbucket 
				https://bitbucket.org/deepak_kakkar123/config-server/src/master/application.yml
				Do we need to re-start the cloud-config-server or microservices after updating application.yml file in git?
			Issue 1. HERE IF I DID NOT START ANY CLOUD-CONFIG-SERVER, USERS/DEPARTMENT SERVICE STILL REGISTER TO SERVICE-REGISTRY, IT IS BECAUSE OF DEFAULT PORT TRYING OF SERVICES
			TO CONNECT TO SERVICE-REGISTRY. SO CHANGE TO 8762 FROM 8761. NOW IT SHOWS ERROR, GREAT. IT MEANS CLOUD-CONFIG-SERVER NEEDS TO BE START TO GET LATEST APPLICATION.YML FILE FROM CLOUD TO GET EUREKA SERVER DETAILS.
			Issue 2. Now issue is coming : No custom http config found for URL: https://bitbucket.org so it still trying to register in 8761 port. Means it was not
			fetching any properties/yml files from GIT/BITBUCKET. So everything is trying to hit on default 8761
				Change: port 8762 in service-registry but also add service-url: defaultZone: http://localhost:8762/eureka. Else it searches peers services on 8761.
			SOLUTIONS: IF NOT USING DEFAULT OORT OF EUREKA
				1. Add eureka client enable with port 8762 in cloud-config as this cloud-config-server is not picking properties from GIT. It is searching for default.
				2. All services that are integrating with cloud-config-server will only get the eureka details from application.yml uploaded in GIT.
				3. Never forget to add in service-registry application.yml file: service-url: defaultZone: http://localhost:8762/eureka
				4. May be all these proble solved by using bootstrap.yml to add 
		8. Centralized configuration: Without restart the apps: actuator/refresh
			TODO:
		9. Distributed Logging:
		10. Login:
		11. Security:
		12. Cache
		13. Message Queue
		10. Distributed Transaction:
				
				
			
			
Reference:
https://www.youtube.com/watch?v=BnknNTN8icw	https://github.com/shabbirdwd53/Springboot-Microservice
https://www.youtube.com/watch?v=sthMcMrspCM
https://www.youtube.com/watch?v=pUFGOngzJig
https://www.youtube.com/watch?v=snZtITKxUBM
	